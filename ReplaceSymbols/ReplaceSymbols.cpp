
#include <iostream>
#include <string>
#include <unordered_map>

std::string replaceChar(std::string str) 
{ 
   std::unordered_map<char, int> Cnt;
    for (char c : str)
    {
        Cnt[tolower(c)]++;// приводим каждый элемент к нижнему регистру если он имел верхний регистр;
    }
    std::string ans = "";
    for (char c : str)
    {
        ans += (Cnt[tolower(c)] > 1) ? ')' : '(';
    }
    return ans;
}

int main()
{
    std::string str1;
    std::cout << "Enter the line:" << std::endl;
    getline(std::cin, str1); // исходная строка, введеная пользователем;
    
    replaceChar(str1);
    std::cout << replaceChar(str1) << std::endl;  
    return 0;
}